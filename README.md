### What is this repository for? ###

* This lisp code merges separate closed polylines into one closed polyline.
* Polylines are converted into regions. Then, the code unions these regions into one region. The merged region is exploded into polylines, and the polylines are finally joined to one polyline. 

![example.png](https://bitbucket.org/repo/Gzxjnz/images/1109229834-example.png)

### How do I get set up? ###
* Copy the lisp code and paste into the command window of AutoCAD.